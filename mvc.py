# Ref 1: https://stackoverflow.com/questions/26698628/mvc-design-with-qt-designer-and-pyqt-pyside
# Ref 2: http://zetcode.com/gui/pyqt5/eventssignals/

import sys
from PyQt5.QtWidgets import QApplication
from model.model import Model
from controller.main_controller import MainController
from view.main_view import MainView

class App(QApplication):
    def __init__(self, sys_argv):
        super(App, self).__init__(sys_argv)
        self.model = Model()
        self.main_controller = MainController(self.model)
        self.main_view = MainView(self.model, self.main_controller)
        self.main_view.show()

if __name__ == "__main__":
    app = App(sys.argv)
    sys.exit(app.exec_())